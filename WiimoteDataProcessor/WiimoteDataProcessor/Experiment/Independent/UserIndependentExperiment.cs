﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteDataProcessor.Gesture;
using WiimoteDataProcessor.Data;
using WiimoteDataProcessor.Types;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor.Experiment.Independent
{
    /// <summary>
    /// An Experiment to measure recognition accuracy with a LinearClassifier trained in 'UserIndpendent' mode.
    /// </summary>
    class UserIndependentExperiment : IExperiment
    {
        /// <summary>
        /// subset of training samples used for training the classifier.
        /// </summary>
        public Dictionary<GestureType, List<GestureSample>> ToTrain;

        /// <summary>
        /// subset of training samples used for recognition.
        /// </summary>
        public Dictionary<GestureType, List<GestureSample>> ToRecognize_Training;
        public Dictionary<GestureType, List<GestureSample>> ToRecognize_CorrectlyClassifiedGameplay;
        public Dictionary<GestureType, List<GestureSample>> ToRecognize_IncorrectlyClassifiedGameplay;

        /// <summary>
        /// The dataset.
        /// </summary>
        private List<UserDataSet> usersData;

        /// <summary>
        /// An instance of the Linear Classifier used in this experiment.
        /// </summary>
        private LinearClassifier Recognizer;

        public UserIndependentExperiment( List<UserDataSet> allData, int no )
        {
            //initialize the dataset
            ToRecognize_CorrectlyClassifiedGameplay = new Dictionary<GestureType, List<GestureSample>>();
            ToRecognize_Training = new Dictionary<GestureType, List<GestureSample>>();
            ToRecognize_IncorrectlyClassifiedGameplay = new Dictionary<GestureType, List<GestureSample>>();
            ToTrain = new Dictionary<GestureType, List<GestureSample>>();
            
            ExperimentNo = no;
            usersData = allData;

            //populate collections that will contain the data
            foreach ( GestureType gesture in Config.GesturesToUse )
            {
                ToRecognize_CorrectlyClassifiedGameplay.Add( gesture, new List<GestureSample>() );
                ToRecognize_IncorrectlyClassifiedGameplay.Add( gesture, new List<GestureSample>() );
                ToRecognize_Training.Add( gesture, new List<GestureSample>() );
                ToTrain.Add( gesture, new List<GestureSample>() );
            }

            PopulateDataSets();

            //train the classifier
            Recognizer = new LinearClassifier( ToTrain );
        }

        private void PopulateDataSets()
        {
            foreach ( UserDataSet uData in usersData )
            {
                //1-construct/prune the 'ToTrain' collection for training the Classifier

                //add all training samples into the 'ToTrain' collection.
                //sort training samples into classes
                foreach ( GestureSample sample in uData.TrainingSamples )
                    if(Config.GesturesToUse.Contains(sample.Gesture))
                        ToTrain[sample.Gesture].Add( sample );
 
                //2-sort correct gameplay samples into classes
                foreach ( GestureSample sample in uData.CorrectlyClassifiedGameplaySamples )
                    if ( Config.GesturesToUse.Contains( sample.Gesture ) )
                        ToRecognize_CorrectlyClassifiedGameplay[sample.Gesture].Add( sample );

                //3-sort possibly correct gameplay samples into classes
                foreach ( GestureSample sample in uData.IncorrectlyClassifiedGameplaySamples )
                    if ( Config.GesturesToUse.Contains( (GestureType)sample.Gesture ) )
                        ToRecognize_IncorrectlyClassifiedGameplay[(GestureType)sample.Gesture].Add( sample );
            }


            //randomely remove excess samples from the 'ToTrain' collection until it meets the specified setting in the experiment configuration.
            //the removed excess samples are put in the 'ToRecognize_Training' for the experiment.
            Random randGenerator = new Random((int)DateTime.Now.Ticks);
            foreach (GestureType gesture in Config.GesturesToUse)
            {
                while ((ToTrain[gesture].Count - Config.NumTrainingSamples) != 0)
                {
                    int randomIndex = randGenerator.Next() % ToTrain[gesture].Count;
                    ToRecognize_Training[gesture].Add(ToTrain[gesture].ElementAt(randomIndex));
                    ToTrain[gesture].RemoveAt(randomIndex);
                }
            }
        }

        private Dictionary<GestureType, Result> DoExperimentOnDataset( Dictionary<GestureType, List<GestureSample>> RecognitionSamples, 
                                                                        string experimentName )
        {
            Dictionary<GestureType, Result> accuracy = new Dictionary<GestureType, Result>();

            ExperimentLog.Instance.Log( "*********************************************************************************" );
            ExperimentLog.Instance.Log( "Running User Independent Experiment("+ExperimentNo+") On :  " + experimentName );

            //do classification on Samples
            foreach ( GestureType gType in Config.GesturesToUse )
            {
                List<GestureSample> samplesToRecognize = RecognitionSamples[gType];
                
                accuracy.Add(gType, new Result());
                accuracy[gType].Total = samplesToRecognize.Count;

                foreach ( GestureSample gSample in samplesToRecognize )
                {
                    GestureType classification = Recognizer.Classify( gSample );
                    if ( classification == gType )
                        accuracy[gType].Correct++;
                }
                
                ExperimentLog.Instance.Log( "Accuracy[" + gType + "] : " + accuracy[gType].Correct + "/" + accuracy[gType].Total + " ~= " + accuracy[gType].Accuracy );

            }

            //report accuracy
            float average = 0.0f;
            float min = float.PositiveInfinity, max = float.NegativeInfinity;
            foreach ( GestureType gType in Config.GesturesToUse )
            {
                average += accuracy[gType].Accuracy;
                if ( accuracy[gType].Accuracy > max )
                    max = accuracy[gType].Accuracy;
                if ( accuracy[gType].Accuracy < min )
                    min = accuracy[gType].Accuracy;
            }

            ExperimentLog.Instance.Log(String.Format( "accuracy: {0}, \t[Min: {1}, Max: {2}]",( average / (float)Config.GesturesToUse.Count ), min, max));
            ExperimentLog.Instance.Log( "*********************************************************************************" );

            return accuracy;
        }

        #region IExperiment Members

        public int ExperimentNo { get; protected set; }

        public Dictionary<GestureType, Result> Accuracy_Correct { get; protected set; }
        public Dictionary<GestureType, Result> Accuracy_InCorrect { get; protected set; }

        public Dictionary<string, Result> UserSpecificAccuracy_Correct { get; protected set; }
        public Dictionary<string, Result> UserSpecificAccuracy_InCorrect { get; protected set; }


        public void Run()
        {
            switch ( Config.DataSetToRecognize )
            {
                case DataType.TrainingSamplesOnly:
                    Accuracy_Correct = DoExperimentOnDataset( ToRecognize_Training, "Training Samples");
                    break;
                case DataType.GameplaySamples:

                    Accuracy_Correct = DoExperimentOnDataset( ToRecognize_CorrectlyClassifiedGameplay, "Correctly Recognized Gameplay Samples");
                    Accuracy_InCorrect = DoExperimentOnDataset( ToRecognize_IncorrectlyClassifiedGameplay, "Incorrectly Recognized Gameplay Samples");

                    break;
            }
        }

        #endregion
    }
}
