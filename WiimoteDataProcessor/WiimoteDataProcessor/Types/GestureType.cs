﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 
 Author: Salman Cheema
 University of Central Florida
 
 Email: salmanc@cs.ucf.edu
 
 Released as part of the 3D Gesture Database analysed in
 
 "Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
 sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
 */


namespace WiimoteDataProcessor.Types
{
    /// <summary>
    /// An enumeration of the different types of supported gestures.
    /// </summary>
    public enum GestureType
    {
        Chop=1,
        Circle=2,
        Infinity=3,
        Forward=4,
        GolfSwing=5,
        Lasso=6,
        Left=7,
        LineDown=8,
        LineToLeft=9,
        LineToRight=10,
        LineUp=11,
        OpenDoor=12,
        Parry=13,
        Right=14,
        Slash=15,
        Slice=16,
        Spike=17,
        Stab=18,
        Square=19,
        Stop=20,
        TennisSwing=21,
        Triangle=22,
        Twister=23,
        Zorro=24,
        Whip=25,
        Unknown=26
    };
}
