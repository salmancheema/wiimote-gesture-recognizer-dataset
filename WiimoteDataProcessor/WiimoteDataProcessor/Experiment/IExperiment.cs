﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteDataProcessor.Gesture;
using WiimoteDataProcessor.Types;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor.Experiment
{
    /// <summary>
    /// An template for each experiment type.
    /// </summary>
    public interface IExperiment
    {
        int ExperimentNo { get; }
        
        /// <summary>
        /// Recognition accuracy grouped by gesture type across all users. This is used to report recognition accuracy in both 'TrainingDataset' and 'CorrectlyClassifiedGameplay' datasets.
        /// </summary>
        Dictionary<GestureType, Result> Accuracy_Correct { get; }
        
        /// <summary>
        /// Recognition accuracy grouped by gesture type across all users. This is used to report recognition accuracy in 'IncorrectlyClassifiedGameplay' dataset.
        /// </summary>
        Dictionary<GestureType, Result> Accuracy_InCorrect { get; }

        /// <summary>
        /// Recognition accuracy grouped by user across all gestures. This is used to report recognition accuracy in both 'TrainingDataset' and 'CorrectlyClassifiedGameplay' datasets.
        /// </summary>
        Dictionary<string, Result> UserSpecificAccuracy_Correct { get; }

        /// <summary>
        /// Recognition accuracy grouped by user across all gestures. This is used to report recognition accuracy in 'IncorrectlyClassifiedGameplay' dataset.
        /// </summary>
        Dictionary<string, Result> UserSpecificAccuracy_InCorrect { get; }

        void Run();
    }
}
