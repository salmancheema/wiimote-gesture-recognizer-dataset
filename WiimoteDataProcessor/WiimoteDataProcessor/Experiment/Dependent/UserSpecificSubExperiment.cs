﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteDataProcessor.Gesture;
using WiimoteDataProcessor.Data;
using WiimoteDataProcessor.Types;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor.Experiment.Dependent
{
    /// <summary>
    /// This is a Sub-Experiment that measures Recognition Accuracy for a LinearClassifier while considering data from a single user.
    /// </summary>
    class UserSpecificSubExperiment
    {
        public int id;
        public Dictionary<GestureType, List<GestureSample>> ToTrain;
        public Dictionary<GestureType, List<GestureSample>> ToRecognize_Training;
        public Dictionary<GestureType, List<GestureSample>> ToRecognize_Correct;
        public Dictionary<GestureType, List<GestureSample>> ToRecognize_PossiblyCorrect;
        public UserDataSet uData;
        public Dictionary<GestureType, Result> RecognitionAccuracy;
        public Dictionary<GestureType, Result> RecognitionAccuracy_Incorrect;

        public LinearClassifier Recognizer;

        public UserSpecificSubExperiment(UserDataSet uDataPt, int uid)
        {
            //initialize the collections that will contain the gesture samples for this user.
            ToRecognize_Correct = new Dictionary<GestureType, List<GestureSample>>();
            ToRecognize_Training = new Dictionary<GestureType, List<GestureSample>>();
            ToRecognize_PossiblyCorrect = new Dictionary<GestureType, List<GestureSample>>();
            ToTrain = new Dictionary<GestureType, List<GestureSample>>();
            RecognitionAccuracy = new Dictionary<GestureType, Result>();
            RecognitionAccuracy_Incorrect = new Dictionary<GestureType, Result>();

            id = uid;
            uData = uDataPt;

            foreach (GestureType gesture in Config.GesturesToUse)
            {
                ToRecognize_Correct.Add(gesture, new List<GestureSample>());
                ToRecognize_PossiblyCorrect.Add(gesture, new List<GestureSample>());
                ToRecognize_Training.Add(gesture, new List<GestureSample>());
                ToTrain.Add(gesture, new List<GestureSample>());
            }

            //initialize the experiment as per the settings in Config.cs
            PopulateDataSets();

            //train the recognizer
            Recognizer = new LinearClassifier(ToTrain);
        }

        private void PopulateDataSets()
        {
            //sort training samples into classes
            foreach (GestureSample sample in uData.TrainingSamples)
                if (Config.GesturesToUse.Contains(sample.Gesture))
                    ToTrain[sample.Gesture].Add(sample);

            //randomely remove excess samples from training data and store them in samples to be recognized
            //this is done until the size of the 'ToTrain' colletion matches the number specified in the experiment configuration.
            Random randGenerator = new Random((int)DateTime.Now.Ticks);
            foreach (GestureType gesture in Config.GesturesToUse)
            {
                while ((ToTrain[gesture].Count - Config.NumTrainingSamples) != 0)
                {
                    int randomIndex = randGenerator.Next() % ToTrain[gesture].Count;
                    ToRecognize_Training[gesture].Add(ToTrain[gesture].ElementAt(randomIndex));
                    ToTrain[gesture].RemoveAt(randomIndex);
                }
            }

            //sort correct gameplay samples into classes
            foreach (GestureSample sample in uData.CorrectlyClassifiedGameplaySamples)
                if (Config.GesturesToUse.Contains(sample.Gesture))
                    ToRecognize_Correct[sample.Gesture].Add(sample);

            //sort possibly correct gameplay samples into classes
            foreach (GestureSample sample in uData.IncorrectlyClassifiedGameplaySamples)
                if (Config.GesturesToUse.Contains(sample.Gesture))
                    ToRecognize_PossiblyCorrect[sample.Gesture].Add(sample);
        }

        public void DoExperiment()
        {
            switch (Config.DataSetToRecognize)
            {
                case DataType.TrainingSamplesOnly:
                    RecognitionAccuracy = DoExperimentOnDataset(ToRecognize_Training, "Training Samples");
                    break;
                case DataType.GameplaySamples:
                    RecognitionAccuracy = DoExperimentOnDataset(ToRecognize_Correct, "Correctly Recognized Gameplay Samples");
                    RecognitionAccuracy_Incorrect = DoExperimentOnDataset(ToRecognize_PossiblyCorrect, "Incorrectly Recognized Gameplay Samples");
                    break;
            }
        }

        private Dictionary<GestureType, Result> DoExperimentOnDataset(Dictionary<GestureType, List<GestureSample>> RecognitionSamples, string experimentName)
        {
            Dictionary<GestureType, Result> expAccuracy = new Dictionary<GestureType, Result>();

            if (Config.ReportIndividualUsersResults)
            {
                ExperimentLog.Instance.Log("********************************************************");
                ExperimentLog.Instance.Log("Experiment for User[" + id + "]:  " + experimentName);
            }

            int totalCorrect = 0, totalSamples = 0;
            float min = float.PositiveInfinity, max = float.NegativeInfinity;

            //do classification 
            foreach (GestureType gType in Config.GesturesToUse)
            {
                List<GestureSample> samplesToRecognize = RecognitionSamples[gType];

                expAccuracy.Add(gType, new Result());
                expAccuracy[gType].Total = samplesToRecognize.Count;

                foreach (GestureSample gSample in samplesToRecognize)
                {
                    GestureType classification = Recognizer.Classify(gSample);
                    if (classification == gType)
                        expAccuracy[gType].Correct++;
                }

                float accuracy = expAccuracy[gType].Accuracy;

                if (Config.ReportIndividualUsersResults)
                    ExperimentLog.Instance.Log("Accuracy[" + gType + "] : " + expAccuracy[gType].Correct + "/" + expAccuracy[gType].Total + " ~= " + accuracy);

                if (accuracy > max)
                    max = accuracy;
                if (accuracy < min)
                    min = accuracy;


                totalCorrect += expAccuracy[gType].Correct;
                totalSamples += expAccuracy[gType].Total;
            }

            if (Config.ReportIndividualUsersResults)
            {
                float avgAccuracy = (float)totalCorrect / (float)totalSamples * 100.0f;
                ExperimentLog.Instance.Log(String.Format("accuracy: {0}, \t[Min: {1}, Max: {2}]", avgAccuracy, min, max));
                ExperimentLog.Instance.Log("********************************************************");
            }

            return expAccuracy;
        }
    }

}
