﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using WiimoteDataProcessor.Gesture;
using WiimoteDataProcessor.Util;
using WiimoteDataProcessor.Experiment;
using WiimoteDataProcessor.Types;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor.Data
{
    /// <summary>
    /// Dataset for a single user. Contains 3 different subsets:
    /// Training samples (625), Correctly Classified Gameplay samples (varies for each user), Incorrectly Classified Gameplay samples (varies for each user)
    /// </summary>
    public class UserDataSet
    {
        /// <summary>
        /// Path where this dataset is stored on disk.
        /// </summary>
        public string Path;

        /// <summary>
        /// Set of training data. 25 samples for each gesture = 625 training samples/user.
        /// </summary>
        public List<GestureSample> TrainingSamples { get; protected set; }

        /// <summary>
        /// Set of gestures that were correctly classified during gameplay.
        /// </summary>
        public List<GestureSample> CorrectlyClassifiedGameplaySamples { get; protected set; }

        /// <summary>
        /// Set of gestures that were incorrectly classified during gameplay.
        /// </summary>
        public List<GestureSample> IncorrectlyClassifiedGameplaySamples { get; protected set; }

        public UserDataSet( string path )
        {
            try
            {
                ///load the dataset for this user.
                this.Path = path;

                //load training data
                this.TrainingSamples = new List<GestureSample>();
                string training_path = path + "\\training\\";
                string[] training_files = Directory.GetFiles( training_path );

                foreach ( string tFile in training_files )
                {
                    GestureSample sample = LoadSample(tFile);
                    this.TrainingSamples.Add( sample );
                }

                //go through gameplay runs and load correct/incorrect gameplay samples
                this.IncorrectlyClassifiedGameplaySamples = new List<GestureSample>();
                this.CorrectlyClassifiedGameplaySamples = new List<GestureSample>();

                string gameplay_path = path + "\\gameplay";
                int count = 0;
                while ( Directory.Exists( gameplay_path + count ) )
                {
                    string ith_gameplay_path = gameplay_path + count + "\\";
                    string[] gameplay_samples = Directory.GetFiles( ith_gameplay_path );
                    foreach ( string gFile in gameplay_samples )
                    {
                        if ( gFile.Contains( ".txt" ) ) continue;

                        GestureSample sample = LoadSample(gFile);

                        if ( gFile.Contains( "-incorrect" ) )
                            this.IncorrectlyClassifiedGameplaySamples.Add( sample );
                        else
                            this.CorrectlyClassifiedGameplaySamples.Add( sample );
                    }
                    ++count;
                }

                ExperimentLog.Instance.Log( "Found " + this.TrainingSamples.Count + " Training, " + this.CorrectlyClassifiedGameplaySamples.Count + " Correct Gameplay, " + this.IncorrectlyClassifiedGameplaySamples.Count + " Incorrect Gameplay at [" + path + "]"  );
            }
            catch ( Exception e )
            {
                ExperimentLog.Instance.Log( e.Message );
                ExperimentLog.Instance.Log( e.StackTrace );
            }
        }

        /// <summary>
        /// Parses a given file and loads the gesture data from it.
        /// </summary>
        /// <param name="filename">path of file containing an instance of a gesture</param>
        /// <returns>gesture sample loaded from the file. Returns null if an I/O error occurs.</returns>
        private GestureSample LoadSample(string filename)
        {
            GestureSample sample = null;

            GestureType gesture = GestureType.Unknown;
            float duration = float.NaN;
            bool rightHanded = false;
            List<Vector3> wiimotePts = new List<Vector3>();
            List<Vector3> motionPlusPts = new List<Vector3>();

            try
            {
                StreamReader reader = File.OpenText(filename);

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    //end of file
                    if(line == null) continue;
                    
                    //skip lines starting with '#' are comments
                    if (line.StartsWith("#")) continue;

                    //ignore empty lines
                    if (line == "") continue;

                    //split the line by the 'space' character
                    string[] tokens = line.Split(" ".ToCharArray());
                    
                    //the first token provides information about the type of data to follow
                    switch (tokens[0])
                    {
                        case "GestureName:":
                            gesture = ReadGestureType(tokens[1]);
                            break;
                        case "Duration(seconds):":
                            duration = float.Parse(tokens[1]);
                            break;
                        case "Handedness:":
                            rightHanded = (tokens[1] == "Right");
                            break;
                        case "WiimotePoints:":
                            int numPoints = int.Parse(tokens[1]);

                            //read the points from succeeding lines.
                            for(int i=0 ; i<numPoints ; ++i)
                            {
                                string point = reader.ReadLine();
                                string[] xyz = point.Split(" ,".ToCharArray());
                                wiimotePts.Add(new Vector3(float.Parse(xyz[0]), float.Parse(xyz[1]), float.Parse(xyz[2])));
                            }

                            break;
                        case "MotionPlusPoints:":
                            int numMotionPlusPoints = int.Parse(tokens[1]);

                            //read datapoints from succeeding lines.
                            for (int i = 0; i < numMotionPlusPoints; ++i)
                            {
                                string point = reader.ReadLine();
                                string[] xyz = point.Split(" ,".ToCharArray());
                                motionPlusPts.Add(new Vector3(float.Parse(xyz[0]), float.Parse(xyz[1]), float.Parse(xyz[2])));
                            }
                            break;
                    }
                }
                
                reader.Close();
                sample = new GestureSample(gesture, rightHanded, duration, wiimotePts, motionPlusPts);
                sample.ComputeFeatures(Config.FeaturesToUse);
            }
            catch (Exception e)
            {
                ExperimentLog.Instance.Log(e.Message);
                ExperimentLog.Instance.Log(e.StackTrace);
            }


            return sample;
        }

        /// <summary>
        /// Converts a gesture name in text to a 'GestureType'.
        /// </summary>
        /// <param name="gestureName"></param>
        /// <returns></returns>
        private GestureType ReadGestureType(string gestureName)
        {
            switch (gestureName)
            {
                case "Chop": return GestureType.Chop;
                case "Circle": return GestureType.Circle;
                case "Infinity": return GestureType.Infinity;
                case "Forward": return GestureType.Forward;
                case "GolfSwing": return GestureType.GolfSwing;
                case "Lasso": return GestureType.Lasso;
                case "Left": return GestureType.Left;
                case "LineDown": return GestureType.LineDown;
                case "LineToLeft": return GestureType.LineToLeft;
                case "LineToRight": return GestureType.LineToRight;
                case "LineUp": return GestureType.LineUp;
                case "OpenDoor": return GestureType.OpenDoor;
                case "Parry": return GestureType.Parry;
                case "Right": return GestureType.Right;
                case "Slash": return GestureType.Slash;
                case "Slice": return GestureType.Slice;
                case "Spike": return GestureType.Spike;
                case "Stab": return GestureType.Stab;
                case "Square": return GestureType.Square;
                case "Stop": return GestureType.Stop;
                case "TennisSwing": return GestureType.TennisSwing;
                case "Triangle": return GestureType.Triangle;
                case "Twister": return GestureType.Twister;
                case "Zorro": return GestureType.Zorro;
                case "Whip": return GestureType.Whip;
            }

            return GestureType.Unknown;
        }


    }
}
