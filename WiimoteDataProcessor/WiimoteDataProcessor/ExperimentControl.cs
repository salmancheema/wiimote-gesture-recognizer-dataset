﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using WiimoteDataProcessor.Gesture;
using WiimoteDataProcessor.Experiment;
using WiimoteDataProcessor.Experiment.Dependent;
using WiimoteDataProcessor.Experiment.Independent;
using WiimoteDataProcessor.Data;
using WiimoteDataProcessor.Types;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor
{
    /// <summary>
    /// Controller class to running the experiments.
    /// </summary>
    class ExperimentControl
    {
        private List<UserDataSet> dataset;

        /// <summary>
        /// Entry point for setting up and running the experiments.
        /// </summary>
        public void RunExperiments()
        {
            //initialize log
            ExperimentLog.Instance.Start();

            //load gesture data and report on the number and type of available samples
            dataset = DataLoader.LoadGestureDataFrom(Config.DataPath);

            int training = 0, incorrect = 0, correct = 0;
            foreach ( UserDataSet user_i in dataset )
            {
                training += user_i.TrainingSamples.Count;
                correct += user_i.CorrectlyClassifiedGameplaySamples.Count;
                incorrect += user_i.IncorrectlyClassifiedGameplaySamples.Count;
            }

            ExperimentLog.Instance.Log( String.Format( "incorrect: {0} , correct: {1}, training: {2}", incorrect, correct, training ) );

            //construct and run experiments as per the configuration
            IExperiment[] experiments = new IExperiment[Config.NumExperiments];
            for(int i=0 ; i<Config.NumExperiments ; ++i)
            {
                switch ( Config.ClassifierTrainingMode )
                {
                    case TrainingMode.UserIndependent:
                        experiments[i] = new UserIndependentExperiment( dataset , i+1);
                        break;
                    case TrainingMode.UserDependent:
                        experiments[i] = new UserDependentExperiment( dataset , i+1);
                        break;
                }

                experiments[i].Run();
            }

            //report merged results
            ReportResults(experiments);

            if (Config.ReportUserSpecificAccuracyResults)
                ReportUserSpecificResults(experiments);
            
            //end log
            ExperimentLog.Instance.End();
        }

        private void ReportResults(IExperiment[] experiments)
        {
            ExperimentLog.Instance.Log("*********************************************************************************");
            ExperimentLog.Instance.Log("************************OVERALL ACCURACY*****************************************");
            ExperimentLog.Instance.Log((Config.DataSetToRecognize == DataType.GameplaySamples ? "Correct Gameplay Samples Recognition Accuracy" : "Training Samples Recognition Accuracy"));
            ExperimentLog.Instance.Log("");

            float overall_accuracy = 0.0f, min = float.PositiveInfinity, max = float.NegativeInfinity;
            foreach (GestureType gType in Config.GesturesToUse)
            {
                float gAccuracy = 0.0f;
                for (int i = 0; i < experiments.Length; ++i)
                    gAccuracy += experiments[i].Accuracy_Correct[gType].Accuracy;

                gAccuracy /= (float)experiments.Length;
                ExperimentLog.Instance.Log("Accuracy[" + gType + "] ~= " + gAccuracy);

                if (gAccuracy > max)
                    max = gAccuracy;
                if (gAccuracy < min)
                    min = gAccuracy;

                overall_accuracy += gAccuracy;
            }

            overall_accuracy /= (float)Config.GesturesToUse.Count;

            ExperimentLog.Instance.Log("");
            ExperimentLog.Instance.Log(String.Format("Overall Accuracy: ~= {0}, \t[Min: {1}, Max:{2}]", overall_accuracy, min, max));


            if (Config.DataSetToRecognize == DataType.GameplaySamples)
            {
                ExperimentLog.Instance.Log("Incorrect Gameplay Samples Recognition Accuracy");
                ExperimentLog.Instance.Log("");

                overall_accuracy = 0.0f;
                min = float.PositiveInfinity;
                max = float.NegativeInfinity;
                foreach (GestureType gType in Config.GesturesToUse)
                {
                    float gAccuracy = 0.0f;
                    for (int i = 0; i < experiments.Length; ++i)
                        gAccuracy += experiments[i].Accuracy_InCorrect[gType].Accuracy;

                    gAccuracy /= (float)experiments.Length;
                    ExperimentLog.Instance.Log("Accuracy[" + gType + "] ~= " + gAccuracy);

                    if (gAccuracy > max)
                        max = gAccuracy;
                    if (gAccuracy < min)
                        min = gAccuracy;

                    overall_accuracy += gAccuracy;
                }

                overall_accuracy /= (float)Config.GesturesToUse.Count ;

                ExperimentLog.Instance.Log("");
                ExperimentLog.Instance.Log(String.Format("Overall Accuracy: ~= {0}, \t[Min: {1}, Max:{2}]", overall_accuracy, min, max));
            }
        }

        private void ReportUserSpecificResults(IExperiment[] experiments)
        {
            ExperimentLog.Instance.LogUserSpecific("************************User Specific Results*****************************************");
            ExperimentLog.Instance.LogUserSpecific((Config.DataSetToRecognize == DataType.GameplaySamples ? "Correct Gameplay Samples Recognition Accuracy" : "Training Samples Recognition Accuracy"));
            ExperimentLog.Instance.LogUserSpecific("");

            
            for(int i=0 ; i<dataset.Count ; ++i)
            {
                string uname = dataset[i].Path;
                float accuracy = 0.0f, min = float.PositiveInfinity, max = float.NegativeInfinity;
                for (int j = 0; j < experiments.Length; ++j)
                {
                    float eAccuracy = experiments[j].UserSpecificAccuracy_Correct[uname].Accuracy;
                    accuracy += eAccuracy;

                    if (eAccuracy > max)
                        max = eAccuracy;
                    if (eAccuracy < min)
                        min = eAccuracy;
                }
                accuracy /= (float)experiments.Length;

                ExperimentLog.Instance.LogUserSpecific(String.Format("User {0} : ~= {1}, \t[Min: {2}, Max:{3}]", i, accuracy, min, max));
            }

            if (Config.DataSetToRecognize == DataType.GameplaySamples)
            {
                ExperimentLog.Instance.LogUserSpecific("");
                ExperimentLog.Instance.LogUserSpecific("Incorrect Gameplay Samples Recognition Accuracy");
                ExperimentLog.Instance.LogUserSpecific("");

                for (int i = 0; i < dataset.Count; ++i)
                {
                    string uname = dataset[i].Path;
                    float accuracy = 0.0f, min = float.PositiveInfinity, max = float.NegativeInfinity;
                    for (int j = 0; j < experiments.Length; ++j)
                    {
                        float eAccuracy = experiments[j].UserSpecificAccuracy_InCorrect[uname].Accuracy;
                        accuracy += eAccuracy;

                        if (eAccuracy > max)
                            max = eAccuracy;
                        if (eAccuracy < min)
                            min = eAccuracy;
                    }
                    accuracy /= (float)experiments.Length;

                    ExperimentLog.Instance.LogUserSpecific(String.Format("User {0} : ~= {1}, \t[Min: {2}, Max:{3}]", i, accuracy, min, max));
                }
            }
        }
    }
}