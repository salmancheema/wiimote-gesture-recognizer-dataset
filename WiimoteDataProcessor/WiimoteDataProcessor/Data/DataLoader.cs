﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WiimoteDataProcessor.Experiment;
using WiimoteDataProcessor.Gesture;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor.Data
{
    /// <summary>
    /// Class to load the dataset. This class can also export the given dataset as '.arff' files for use in WEKA.
    /// </summary>
    class DataLoader
    {
        /// <summary>
        /// Loads the gesture dataset from a given location.
        /// </summary>
        /// <param name="dataPath">path from which to load data.</param>
        /// <returns>returns the dataset. returns empty list if no data found or some error occurs.</returns>
        public static List<UserDataSet> LoadGestureDataFrom(string dataPath)
        {
            List<UserDataSet> gestureData = new List<UserDataSet>();
            try
            {
                if (!Directory.Exists(dataPath))
                {
                    ExperimentLog.Instance.Log("Unable to find User Data Directory");
                    return gestureData;
                }

                string[] usernames = Directory.GetDirectories(dataPath);
                if (usernames.Length == 0)
                {
                    ExperimentLog.Instance.Log("Found NO User Data at [" + dataPath + "]");
                    return gestureData;
                }

                ExperimentLog.Instance.Log("Found Data for " + usernames.Length + " participants");

                for (int i = 0; i < usernames.Length; ++i)
                    gestureData.Add(new UserDataSet(usernames[i]));
            }
            catch (Exception e)
            {
                ExperimentLog.Instance.Log(e.Message);
                ExperimentLog.Instance.Log(e.StackTrace);
            }
            return gestureData;
        }

        /// <summary>
        /// Loads a given dataset, and exports each gesture sample's feature vector as '.arff' files to be analyzed by WEKA.
        /// For each user, 3 '.arff' files are generated, pertaining to training data, correctly classifier gameplay data, and incorrently classified gameplay data.
        /// </summary>
        /// <param name="dataPath">Source path for dataset.</param>
        /// <param name="outputPath">Target path for exporting dataset.</param>
        public static void ExportFeaturesAsARFF(string dataPath, string outputPath)
        {
            ExperimentLog.Instance.Start();

            //load the experiment data
            List<UserDataSet> data = LoadGestureDataFrom(dataPath);

            foreach (UserDataSet data_i in data)
            {
                int index = data_i.Path.LastIndexOf("\\");
                string name = data_i.Path.Substring(index + 1, data_i.Path.Length - index - 1);

                //export the three different lists of gestures for each user in a separate file.
                string filename_prefix = outputPath + name + "-";
                WriteARFF(name, filename_prefix + "training.arff", data_i.TrainingSamples);
                WriteARFF(name, filename_prefix + "correct_gameplay.arff", data_i.CorrectlyClassifiedGameplaySamples);
                WriteARFF(name, filename_prefix + "incorrect_gameplay.arff", data_i.IncorrectlyClassifiedGameplaySamples);
            }

            ExperimentLog.Instance.End();
        }

        /// <summary>
        /// Writes the feature vectors for a list of gestures to an '.arff' file.
        /// </summary>
        /// <param name="userid">username for user who recorded the gestures.</param>
        /// <param name="filename">path of output file.</param>
        /// <param name="samples">list of gestures.</param>
        private static void WriteARFF(string userid, string filename, List<GestureSample> samples)
        {
            //create file
            StreamWriter file = File.CreateText(filename);

            //write arff header
            file.Write("@RELATION " + filename + "\r\r");

            //file.Write("@ATTRIBUTE username STRING\r");
            file.Write("@ATTRIBUTE GestureType  {Chop,Circle,Infinity,Forward,GolfSwing,Lasso,Left,LineDown,LineToLeft,LineToRight,LineUp,OpenDoor,Parry,Right,Slash,Slice,Spike,Stab,Square,Stop,TennisSwing,Triangle,Twister,Zorro,Whip}\r");
            file.Write("@ATTRIBUTE MinX NUMERIC\r");
            file.Write("@ATTRIBUTE MinY NUMERIC\r");
            file.Write("@ATTRIBUTE MinZ NUMERIC\r");
            file.Write("@ATTRIBUTE MaxX NUMERIC\r");
            file.Write("@ATTRIBUTE MaxY NUMERIC\r");
            file.Write("@ATTRIBUTE MaxZ NUMERIC\r");
            file.Write("@ATTRIBUTE MeanX NUMERIC\r");
            file.Write("@ATTRIBUTE MeanY NUMERIC\r");
            file.Write("@ATTRIBUTE MeanZ NUMERIC\r");
            file.Write("@ATTRIBUTE MedianX NUMERIC\r");
            file.Write("@ATTRIBUTE MedianY NUMERIC\r");
            file.Write("@ATTRIBUTE MedianZ NUMERIC\r");
            file.Write("@ATTRIBUTE DiagonalLength NUMERIC\r");
            file.Write("@ATTRIBUTE StartAngleSinXY NUMERIC\r");
            file.Write("@ATTRIBUTE StartAngleCosXY NUMERIC\r");
            file.Write("@ATTRIBUTE StartAngleSinXZ NUMERIC\r");
            file.Write("@ATTRIBUTE FirstLastAngleSinXY NUMERIC\r");
            file.Write("@ATTRIBUTE FirstLastAngleCosXY NUMERIC\r");
            file.Write("@ATTRIBUTE FirstLastAngleSinXZ NUMERIC\r");
            file.Write("@ATTRIBUTE TotalAngleXY NUMERIC\r");
            file.Write("@ATTRIBUTE TotalAngleXZ NUMERIC\r");
            file.Write("@ATTRIBUTE AbsoluteTotalAngleXY NUMERIC\r");
            file.Write("@ATTRIBUTE AbsoluteTotalAngleXZ NUMERIC\r");
            file.Write("@ATTRIBUTE SquaredTotalAngleXY NUMERIC\r");
            file.Write("@ATTRIBUTE SquaredTotalAngleXZ NUMERIC\r");
            file.Write("@ATTRIBUTE FirstLastDistance NUMERIC\r");
            file.Write("@ATTRIBUTE TotalDistance NUMERIC\r");
            file.Write("@ATTRIBUTE MaxAccelerationSquared NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MinX NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MinY NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MinZ NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MaxX NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MaxY NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MaxZ NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MeanX NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MeanY NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MeanZ NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MedianX NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MedianY NUMERIC\r");
            file.Write("@ATTRIBUTE MotionPlus_MedianZ NUMERIC\r");
            file.Write("@ATTRIBUTE Duration NUMERIC\r");


            //write out the feature vector for this sample
            file.Write("\r@DATA\r");
            foreach (GestureSample gs in samples)
            {
                string datum = gs.Gesture.ToString();
                for (int i = 0; i < gs.Features.Length; ++i)
                    datum += "," + gs.Features[i];
                file.Write(datum + "\r");
            }

            file.Flush();
            file.Close();
        }
    }
}
