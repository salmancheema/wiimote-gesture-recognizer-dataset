﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteDataProcessor.Experiment;
using WiimoteDataProcessor.Data;

namespace WiimoteDataProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            ExperimentControl exp = new ExperimentControl();
            exp.RunExperiments();

            //DataLoader.ExportFeaturesAsARFF(Config.DataPath, Config.WekaOutputPath);
        }
    }
}
