﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteDataProcessor.Gesture;
using WiimoteDataProcessor.Experiment;
using WiimoteDataProcessor.Types;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor
{
    /// <summary>
    /// configuration settings for training/recognition/experiment setup.
    /// </summary>
    class Config
    {
        /// <summary>
        /// Source location for dataset to be used for experiment.
        /// </summary>
        public static string DataPath = @"..\..\..\..\data_pruned";

        /// <summary>
        /// Target location where experiment results will be stored as text files.
        /// </summary>
        public static string ResultsPath = @"..\..\..\..\results\";

        /// <summary>
        /// Target location where the dataset will be exported as '.arff' files to be used for weka.
        /// </summary>
        public static string WekaOutputPath = @"..\..\..\..\data_arff\";

        /// <summary>
        /// Number of training samples per gesture to be used for the experiment. 
        /// In 'UserDependent' mode, this can range between 0-25, as the number of available samples is 25 samples per user per gesture.
        /// In 'UserIndependent' mode, this can range between 0-625, as the number of available samples is 625 samples for each gesture (from all 25 users).
        /// </summary>
        public static int NumTrainingSamples = 10;

        /// <summary>
        /// Number of times to run the experiment with random sub-selections of the training data.
        /// </summary>
        public static int NumExperiments = 20;

        public static bool ReportUserSpecificAccuracyResults = false;
        public static bool ReportIndividualUsersResults = false;

        public static TrainingMode ClassifierTrainingMode = TrainingMode.UserDependent;
        public static GestureFeatures FeaturesToUse = GestureFeatures.RawAccelerationWithMotionPlus;
        public static DataType DataSetToRecognize = DataType.TrainingSamplesOnly;
        
        /// <summary>
        /// Can be used to select a subset of available gestures for the experiment.
        /// </summary>
        public static List<GestureType> GesturesToUse = new List<GestureType> ();

        /// <summary>
        /// The number of features are dependent on whether only Wiimote features are to be used (29 features) or whether both Wiimote and MotionPlus (29 + 12) features are to be used.
        /// </summary>
        public static int NumFeatures
        {
            get
            {
                switch (FeaturesToUse)
                {
                    case GestureFeatures.RawAcceleration:
                        return 29;
                }
                //number of features increase when the MotionPlus attachment is used. for details, see the 'XYZFeatures' class
                return 41;
            }
        }
    
        static Config()
        {
            GesturesToUse.Add(GestureType.Chop);
            GesturesToUse.Add(GestureType.Circle);
            GesturesToUse.Add(GestureType.Forward);
            GesturesToUse.Add(GestureType.GolfSwing);
            GesturesToUse.Add(GestureType.Infinity);
            GesturesToUse.Add(GestureType.Lasso);
            GesturesToUse.Add(GestureType.Left);
            GesturesToUse.Add(GestureType.LineDown);
            GesturesToUse.Add(GestureType.LineToLeft);
            GesturesToUse.Add(GestureType.LineToRight);
            GesturesToUse.Add(GestureType.LineUp);
            GesturesToUse.Add(GestureType.OpenDoor);
            GesturesToUse.Add(GestureType.Parry);
            GesturesToUse.Add(GestureType.Right);
            GesturesToUse.Add(GestureType.Slash);
            GesturesToUse.Add(GestureType.Slice);
            GesturesToUse.Add(GestureType.Spike);
            GesturesToUse.Add(GestureType.Square);
            GesturesToUse.Add(GestureType.Stab);
            GesturesToUse.Add(GestureType.Stop);
            GesturesToUse.Add(GestureType.TennisSwing);
            GesturesToUse.Add(GestureType.Triangle);
            GesturesToUse.Add(GestureType.Twister);
            GesturesToUse.Add(GestureType.Whip);
            GesturesToUse.Add(GestureType.Zorro);
        }


        public static void LogDescription()
        {
            ExperimentLog.Instance.Log( "Running Experiment in '" + ( ClassifierTrainingMode == TrainingMode.UserDependent ? "User Dependent" : "User Independent" ) + "' Mode" );
            ExperimentLog.Instance.Log( "Recognition Sample Type: " + ( DataSetToRecognize == DataType.TrainingSamplesOnly ? "Training Samples" : "Gameplay Samples" ) );
            ExperimentLog.Instance.Log( "Using [" + NumTrainingSamples + "] Samples per Gesture for training" );
            ExperimentLog.Instance.Log( "Report Individual Users Results: " + (ReportIndividualUsersResults? "Yes" : "No") );
            ExperimentLog.Instance.Log("Number of Experiments to Test Scenario: "+NumExperiments);
         
            ExperimentLog.Instance.Log("Experiment being run on "+GesturesToUse.Count+" Gestures");

            foreach ( GestureType gType in GesturesToUse )
                ExperimentLog.Instance.Log(gType.ToString());
        }
    }
}
