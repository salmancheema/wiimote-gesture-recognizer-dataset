﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor.Experiment
{
    /// <summary>
    /// A singleton wrapper class for the experiment log.
    /// </summary>
    class ExperimentLog
    {
        private StreamWriter experiment_log, user_specific_log;
        private DateTime beginTime, endTime;

        private static ExperimentLog _instance;
        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        public static ExperimentLog Instance
        {
            get
            {
                if ( _instance == null ) _instance = new ExperimentLog();
                return _instance;
            }
        }


        private ExperimentLog()
        {
        }

        /// <summary>
        /// The log file must be started manually. It will then construct a unique name and create a text file as per the experiment settings.
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            try
            {
                //log starting time and set up directory structure
                beginTime = DateTime.Now;
                if (!Directory.Exists(Config.ResultsPath))
                    Directory.CreateDirectory(Config.ResultsPath);

                //create unique name
                string name = Config.ClassifierTrainingMode.ToString() + "_" + Config.DataSetToRecognize.ToString() + "_" +Config.GesturesToUse.Count +"Gestures_" + Config.NumTrainingSamples + "Samples";
                string path = Config.ResultsPath + "" + name + "_";
                int count = 0;
                while ( File.Exists( path + count + ".txt" ) ) count++;
                path +=  count + ".txt";

                //create log file(s)
                experiment_log = File.CreateText( path );

                if (Config.ReportUserSpecificAccuracyResults)
                    user_specific_log = File.CreateText(Config.ResultsPath + name + "_UserSpecificResults" + count + ".txt"); 

                Log( "Began Log [" + path + "] at " + beginTime.ToString() );

                //log experiment settings
                Config.LogDescription();
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.Message );
                Console.WriteLine( e.StackTrace );
                return false;
            }
            return true;
        }

        /// <summary>
        /// Logs a message to both the console and the main log file.
        /// </summary>
        /// <param name="message"></param>
        public void Log( string message )
        {
            Console.WriteLine( message );
            experiment_log.WriteLine( message );
        }

        /// <summary>
        /// Logs a message to the user-specific log.
        /// </summary>
        /// <param name="msg"></param>
        public void LogUserSpecific(string msg)
        {
            user_specific_log.WriteLine(msg);
        }

        /// <summary>
        /// The logging must be terminated properly, otherwise the results may not be fully written to the file.
        /// </summary>
        public void End()
        {
            endTime = DateTime.Now;

            Log( "Experiment Completed in " + ( endTime - beginTime ).TotalMinutes + " minutes" );

            experiment_log.Flush();
            experiment_log.Close();

            if (user_specific_log != null)
            {
                user_specific_log.Flush();
                user_specific_log.Close();
            }
        }
    }
}
