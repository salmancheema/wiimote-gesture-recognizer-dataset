﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteDataProcessor.Util;
using WiimoteDataProcessor.Types;

/*
 
 Author: Salman Cheema
 University of Central Florida
 
 Email: salmanc@cs.ucf.edu
 
 Released as part of the 3D Gesture Database analysed in
 
 "Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
 sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
 */

namespace WiimoteDataProcessor.Gesture
{
    /// <summary>
    /// Contains the datapoints corresponding to a single instance of a gesture. 
    /// </summary>
    public class GestureSample
    {
        /// <summary>
        /// Type of Gesture.
        /// </summary>
        public GestureType Gesture { get; protected set; }

        /// <summary>
        /// Gesture duration in seconds.
        /// </summary>
        public float Duration { get; protected set; }

        /// <summary>
        /// List of acceleration values sampled from a Nintendo Wiimote.
        /// </summary>
        public List<Vector3> WiimotePoints { get; protected set; }

        /// <summary>
        /// List of angular velocity values sampled form a MotionPlus attached to a Nintendo Wiimote.
        /// </summary>
        public List<Vector3> MotionPlusPoints { get; protected set; }

        /// <summary>
        /// A boolean flag indicating whether this gesture was performed with the Right or Left hand.
        /// </summary>
        public bool RightHanded { get; protected set; }

        /// <summary>
        /// A list of features computed from the data points.
        /// </summary>
        public float[] Features { get; protected set; }


        public GestureSample(GestureType gType, bool rightHanded, float duration, List<Vector3> wiimotePts, List<Vector3> motionPlusPts)
        {
            Gesture = gType;
            Duration = duration;
            RightHanded = rightHanded;

            WiimotePoints = new List<Vector3>();
            WiimotePoints.AddRange(wiimotePts);

            MotionPlusPoints = new List<Vector3>();
            MotionPlusPoints.AddRange(motionPlusPts);
        }

        /// <summary>
        /// Will recompute the feature vector.
        /// </summary>
        /// <param name="typeOfFeaturesToCompute">indicates the type of features to be computed.</param>
        public void ComputeFeatures( GestureFeatures typeOfFeaturesToCompute)
        {
            List<float> allFeatures = new List<float>();
            XYZFeatures wiimote = null, motionplus = null;

            switch (typeOfFeaturesToCompute)
            {
                case GestureFeatures.RawAcceleration:
                    wiimote = new XYZFeatures(WiimotePoints);
                    break;
                case GestureFeatures.RawAccelerationWithMotionPlus:
                    wiimote = new XYZFeatures(WiimotePoints);
                    motionplus = new XYZFeatures(MotionPlusPoints);
                    break;
            }

            allFeatures.AddRange(wiimote.NormalFeatures);
            if (motionplus != null)
                allFeatures.AddRange(motionplus.MotionPlusFeatures);
            
            allFeatures.Add(this.Duration);
            Features = allFeatures.ToArray();
        }
    }
}
