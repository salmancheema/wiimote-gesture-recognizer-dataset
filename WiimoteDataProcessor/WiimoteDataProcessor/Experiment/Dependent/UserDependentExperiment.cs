﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WiimoteDataProcessor.Gesture;
using WiimoteDataProcessor.Data;
using WiimoteDataProcessor.Types;

/*
 
Author: Salman Cheema
University of Central Florida
 
Email: salmanc@cs.ucf.edu
 
Released as part of the 3D Gesture Database analysed in
 
"Salman Cheema, Michael Hoffman, Joseph J. LaViola Jr., 3D Gesture classification with linear acceleration and angular velocity 
sensing devices for video games, Entertainment Computing, Volume 4, Issue 1, February 2013, Pages 11-24, ISSN 1875-9521, 10.1016/j.entcom.2012.09.002"
 
*/

namespace WiimoteDataProcessor.Experiment.Dependent
{
    /// <summary>
    /// An experiment to measure Recognition Accuracy with LinearClassifiers trained in 'UserDependent' mode.
    /// </summary>
    class UserDependentExperiment : IExperiment
    {
        /// <summary>
        /// A list of sub-experiments. As this is a 'UserDependent' experiment, we will train a Classifier for each user and then average
        /// the results to get the overall accuracy.
        /// </summary>
        private List<UserSpecificSubExperiment> data;

        public UserDependentExperiment( List<UserDataSet> allData, int no )
        {
            ExperimentNo = no;
            data = new List<UserSpecificSubExperiment>();
            for ( int i = 0; i < allData.Count; ++i )
                data.Add( new UserSpecificSubExperiment( allData[i], i) );
        }

        #region IExperiment Members

        public int ExperimentNo { get; protected set; }
        public Dictionary<GestureType, Result> Accuracy_Correct { get; protected set; }
        public Dictionary<GestureType, Result> Accuracy_InCorrect { get; protected set; }
        public Dictionary<string, Result> UserSpecificAccuracy_Correct { get; protected set; }
        public Dictionary<string, Result> UserSpecificAccuracy_InCorrect { get; set; }

        /// <summary>
        /// Starts the experiment.
        /// </summary>
        public void Run()
        {
            //Run all the sub experiments
            foreach ( UserSpecificSubExperiment udi in data )
                udi.DoExperiment();

            //Report accuracy figures
            ExperimentLog.Instance.Log( "*********************************************************************************" );
            ExperimentLog.Instance.Log( "*******************************   MERGED RESULTS(" + ExperimentNo+ ")   ***************************" );

            //initialize accuracy figures for Correct/Incorrect gestures
            //if the experiment is set up to work on Training samples, the 'Incorrect' dictionary will not be used.
            //The 'Accuracy_Incorrect' dictionary is only used for gameplay gestures
            Accuracy_Correct = new Dictionary<GestureType, Result>();
            Accuracy_InCorrect = new Dictionary<GestureType, Result>();


            ExperimentLog.Instance.Log( ( Config.DataSetToRecognize == DataType.GameplaySamples ? "Correct Gameplay Samples Recognition Accuracy" : "Training Samples Recognition Accuracy" ) );
            ExperimentLog.Instance.Log( "" );

            //STEP 1 - Report accuracy figures for 'correctly recognized gameplay'/'training' samples
            //initialize overall accuracy counters
            float overall_accuracy = 0.0f, min = float.PositiveInfinity, max = float.NegativeInfinity;
            int correct = 0, total = 0;
            foreach ( GestureType gType in Config.GesturesToUse )
            {
                Accuracy_Correct.Add(gType, new Result());
                foreach ( UserSpecificSubExperiment udi in data )
                {
                    Accuracy_Correct[gType].Correct += udi.RecognitionAccuracy[gType].Correct;
                    Accuracy_Correct[gType].Total += udi.RecognitionAccuracy[gType].Total;
                }

                float gesture_accuracy = Accuracy_Correct[gType].Accuracy;
                ExperimentLog.Instance.Log( "Accuracy[" + gType + "] : " + Accuracy_Correct[gType].Correct + "/" + Accuracy_Correct[gType].Total + " ~= " + gesture_accuracy );

                if ( gesture_accuracy > max )
                    max = gesture_accuracy;
                if ( gesture_accuracy < min )
                    min = gesture_accuracy;

                correct += Accuracy_Correct[gType].Correct;
                total += Accuracy_Correct[gType].Total;
            }

            overall_accuracy = (float)correct * 100.0f / (float)total;

            ExperimentLog.Instance.Log( "" );
            ExperimentLog.Instance.Log( String.Format( "Overall Accuracy: {0}/{1} ~= {2}, \t[Min: {3}, Max:{4}]", correct, total, overall_accuracy, min, max ) );

            //STEP 2: Report accuracy for the incorrectly recognized gameplay gesture samples
            //This is optional and will only happen when the experiment is set to work on gameplay gestures.
            if ( Config.DataSetToRecognize == DataType.GameplaySamples )
            {
                ExperimentLog.Instance.Log( "Incorrect Gameplay Samples Recognition Accuracy" );
                ExperimentLog.Instance.Log( "" );

                overall_accuracy = 0.0f;
                min = float.PositiveInfinity;
                max = float.NegativeInfinity;
                total = correct = 0;
                foreach ( GestureType gType in Config.GesturesToUse )
                {
                    Accuracy_InCorrect.Add(gType, new Result());
                    foreach ( UserSpecificSubExperiment udi in data )
                    {
                        Accuracy_InCorrect[gType].Correct += udi.RecognitionAccuracy_Incorrect[gType].Correct;
                        Accuracy_InCorrect[gType].Total += udi.RecognitionAccuracy_Incorrect[gType].Total;
                    }

                    float gesture_accuracy = Accuracy_InCorrect[gType].Accuracy;//(float)gCorrect * 100.0f / (float)gTotal;
                    ExperimentLog.Instance.Log("Accuracy[" + gType + "] : " + Accuracy_InCorrect[gType].Correct + "/" + Accuracy_InCorrect[gType].Total + " ~= " + gesture_accuracy);

                    if ( gesture_accuracy > max )
                        max = gesture_accuracy;
                    if ( gesture_accuracy < min )
                        min = gesture_accuracy;

                    correct += Accuracy_InCorrect[gType].Correct;
                    total += Accuracy_InCorrect[gType].Total;
                }

                //Report overall accuracy for the entire dataset
                overall_accuracy = (float)correct * 100.0f / (float)total;

                ExperimentLog.Instance.Log( "" );
                ExperimentLog.Instance.Log( String.Format( "Overall Accuracy: {0}/{1} ~= {2}, \t[Min: {3}, Max:{4}]", correct, total, overall_accuracy, min, max ) );
            }

            ComputeUserSpecificAccuracyFigures();
        }

        private void ComputeUserSpecificAccuracyFigures()
        {
            UserSpecificAccuracy_Correct = new Dictionary<string, Result>();
            UserSpecificAccuracy_InCorrect = new Dictionary<string, Result>();

            foreach (UserSpecificSubExperiment usdi in data)
            {
                Result rC = new Result();
                Result rI = new Result();

                foreach (GestureType gtype in Config.GesturesToUse)
                {
                    rC.Correct += usdi.RecognitionAccuracy[gtype].Correct;
                    rC.Total += usdi.RecognitionAccuracy[gtype].Total;

                    if (Config.DataSetToRecognize == DataType.GameplaySamples)
                    {
                        rI.Correct += usdi.RecognitionAccuracy_Incorrect[gtype].Correct;
                        rI.Total += usdi.RecognitionAccuracy_Incorrect[gtype].Total;
                    }
                }

                UserSpecificAccuracy_Correct.Add(usdi.uData.Path, rC);
                UserSpecificAccuracy_InCorrect.Add(usdi.uData.Path, rI);
            }
        }

        #endregion

    }
}
