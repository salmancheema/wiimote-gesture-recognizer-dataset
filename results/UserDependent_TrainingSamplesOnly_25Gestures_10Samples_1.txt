Began Log [..\..\..\..\results\UserDependent_TrainingSamplesOnly_25Gestures_10Samples_1.txt] at 8/24/2015 3:31:44 PM
Running Experiment in 'User Dependent' Mode
Recognition Sample Type: Training Samples
Using [10] Samples per Gesture for training
Report Individual Users Results: No
Number of Experiments to Test Scenario: 20
Experiment being run on 25 Gestures
Chop
Circle
Forward
GolfSwing
Infinity
Lasso
Left
LineDown
LineToLeft
LineToRight
LineUp
OpenDoor
Parry
Right
Slash
Slice
Spike
Square
Stab
Stop
TennisSwing
Triangle
Twister
Whip
Zorro
Found Data for 25 participants
Found 625 Training, 59 Correct Gameplay, 8 Incorrect Gameplay at [..\..\..\..\data_pruned\u003]
Found 625 Training, 60 Correct Gameplay, 14 Incorrect Gameplay at [..\..\..\..\data_pruned\u004]
Found 625 Training, 58 Correct Gameplay, 13 Incorrect Gameplay at [..\..\..\..\data_pruned\u005]
Found 625 Training, 56 Correct Gameplay, 22 Incorrect Gameplay at [..\..\..\..\data_pruned\u006]
Found 625 Training, 58 Correct Gameplay, 22 Incorrect Gameplay at [..\..\..\..\data_pruned\u007]
Found 625 Training, 54 Correct Gameplay, 15 Incorrect Gameplay at [..\..\..\..\data_pruned\u008]
Found 625 Training, 59 Correct Gameplay, 25 Incorrect Gameplay at [..\..\..\..\data_pruned\u009]
Found 625 Training, 54 Correct Gameplay, 33 Incorrect Gameplay at [..\..\..\..\data_pruned\u010]
Found 625 Training, 60 Correct Gameplay, 20 Incorrect Gameplay at [..\..\..\..\data_pruned\u011]
Found 625 Training, 59 Correct Gameplay, 24 Incorrect Gameplay at [..\..\..\..\data_pruned\u012]
Found 625 Training, 58 Correct Gameplay, 8 Incorrect Gameplay at [..\..\..\..\data_pruned\u013]
Found 625 Training, 57 Correct Gameplay, 27 Incorrect Gameplay at [..\..\..\..\data_pruned\u014]
Found 625 Training, 59 Correct Gameplay, 15 Incorrect Gameplay at [..\..\..\..\data_pruned\u015]
Found 625 Training, 52 Correct Gameplay, 31 Incorrect Gameplay at [..\..\..\..\data_pruned\u016]
Found 625 Training, 59 Correct Gameplay, 16 Incorrect Gameplay at [..\..\..\..\data_pruned\u017]
Found 625 Training, 60 Correct Gameplay, 14 Incorrect Gameplay at [..\..\..\..\data_pruned\u018]
Found 625 Training, 57 Correct Gameplay, 18 Incorrect Gameplay at [..\..\..\..\data_pruned\u019]
Found 625 Training, 52 Correct Gameplay, 36 Incorrect Gameplay at [..\..\..\..\data_pruned\u020]
Found 625 Training, 51 Correct Gameplay, 29 Incorrect Gameplay at [..\..\..\..\data_pruned\u021]
Found 625 Training, 59 Correct Gameplay, 22 Incorrect Gameplay at [..\..\..\..\data_pruned\u022]
Found 625 Training, 60 Correct Gameplay, 18 Incorrect Gameplay at [..\..\..\..\data_pruned\u023]
Found 625 Training, 60 Correct Gameplay, 22 Incorrect Gameplay at [..\..\..\..\data_pruned\u024]
Found 625 Training, 54 Correct Gameplay, 14 Incorrect Gameplay at [..\..\..\..\data_pruned\u025]
Found 625 Training, 57 Correct Gameplay, 28 Incorrect Gameplay at [..\..\..\..\data_pruned\u026]
Found 625 Training, 60 Correct Gameplay, 6 Incorrect Gameplay at [..\..\..\..\data_pruned\u027]
incorrect: 500 , correct: 1432, training: 15625
*********************************************************************************
*******************************   MERGED RESULTS(1)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 373/375 ~= 99.46667
Accuracy[Circle] : 371/375 ~= 98.93333
Accuracy[Forward] : 372/375 ~= 99.2
Accuracy[GolfSwing] : 371/375 ~= 98.93333
Accuracy[Infinity] : 370/375 ~= 98.66666
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 373/375 ~= 99.46667
Accuracy[LineDown] : 373/375 ~= 99.46667
Accuracy[LineToLeft] : 369/375 ~= 98.4
Accuracy[LineToRight] : 372/375 ~= 99.2
Accuracy[LineUp] : 373/375 ~= 99.46667
Accuracy[OpenDoor] : 373/375 ~= 99.46667
Accuracy[Parry] : 375/375 ~= 100
Accuracy[Right] : 366/375 ~= 97.6
Accuracy[Slash] : 374/375 ~= 99.73333
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 372/375 ~= 99.2
Accuracy[Square] : 374/375 ~= 99.73333
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 372/375 ~= 99.2
Accuracy[TennisSwing] : 367/375 ~= 97.86667
Accuracy[Triangle] : 367/375 ~= 97.86667
Accuracy[Twister] : 372/375 ~= 99.2
Accuracy[Whip] : 373/375 ~= 99.46667
Accuracy[Zorro] : 374/375 ~= 99.73333

Overall Accuracy: 9292/9375 ~= 99.11467, 	[Min: 97.6, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(2)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 370/375 ~= 98.66666
Accuracy[Circle] : 371/375 ~= 98.93333
Accuracy[Forward] : 371/375 ~= 98.93333
Accuracy[GolfSwing] : 370/375 ~= 98.66666
Accuracy[Infinity] : 372/375 ~= 99.2
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 374/375 ~= 99.73333
Accuracy[LineDown] : 373/375 ~= 99.46667
Accuracy[LineToLeft] : 370/375 ~= 98.66666
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 373/375 ~= 99.46667
Accuracy[OpenDoor] : 374/375 ~= 99.73333
Accuracy[Parry] : 373/375 ~= 99.46667
Accuracy[Right] : 373/375 ~= 99.46667
Accuracy[Slash] : 374/375 ~= 99.73333
Accuracy[Slice] : 372/375 ~= 99.2
Accuracy[Spike] : 374/375 ~= 99.73333
Accuracy[Square] : 374/375 ~= 99.73333
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 372/375 ~= 99.2
Accuracy[TennisSwing] : 371/375 ~= 98.93333
Accuracy[Triangle] : 368/375 ~= 98.13333
Accuracy[Twister] : 369/375 ~= 98.4
Accuracy[Whip] : 372/375 ~= 99.2
Accuracy[Zorro] : 374/375 ~= 99.73333

Overall Accuracy: 9299/9375 ~= 99.18933, 	[Min: 98.13333, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(3)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 371/375 ~= 98.93333
Accuracy[Circle] : 370/375 ~= 98.66666
Accuracy[Forward] : 369/375 ~= 98.4
Accuracy[GolfSwing] : 367/375 ~= 97.86667
Accuracy[Infinity] : 370/375 ~= 98.66666
Accuracy[Lasso] : 363/375 ~= 96.8
Accuracy[Left] : 372/375 ~= 99.2
Accuracy[LineDown] : 372/375 ~= 99.2
Accuracy[LineToLeft] : 374/375 ~= 99.73333
Accuracy[LineToRight] : 373/375 ~= 99.46667
Accuracy[LineUp] : 374/375 ~= 99.73333
Accuracy[OpenDoor] : 374/375 ~= 99.73333
Accuracy[Parry] : 373/375 ~= 99.46667
Accuracy[Right] : 371/375 ~= 98.93333
Accuracy[Slash] : 373/375 ~= 99.46667
Accuracy[Slice] : 374/375 ~= 99.73333
Accuracy[Spike] : 372/375 ~= 99.2
Accuracy[Square] : 372/375 ~= 99.2
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 371/375 ~= 98.93333
Accuracy[TennisSwing] : 375/375 ~= 100
Accuracy[Triangle] : 374/375 ~= 99.73333
Accuracy[Twister] : 373/375 ~= 99.46667
Accuracy[Whip] : 371/375 ~= 98.93333
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9295/9375 ~= 99.14667, 	[Min: 96.8, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(4)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 371/375 ~= 98.93333
Accuracy[Circle] : 369/375 ~= 98.4
Accuracy[Forward] : 369/375 ~= 98.4
Accuracy[GolfSwing] : 371/375 ~= 98.93333
Accuracy[Infinity] : 371/375 ~= 98.93333
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 373/375 ~= 99.46667
Accuracy[LineToLeft] : 366/375 ~= 97.6
Accuracy[LineToRight] : 373/375 ~= 99.46667
Accuracy[LineUp] : 372/375 ~= 99.2
Accuracy[OpenDoor] : 373/375 ~= 99.46667
Accuracy[Parry] : 373/375 ~= 99.46667
Accuracy[Right] : 374/375 ~= 99.73333
Accuracy[Slash] : 374/375 ~= 99.73333
Accuracy[Slice] : 372/375 ~= 99.2
Accuracy[Spike] : 367/375 ~= 97.86667
Accuracy[Square] : 375/375 ~= 100
Accuracy[Stab] : 371/375 ~= 98.93333
Accuracy[Stop] : 369/375 ~= 98.4
Accuracy[TennisSwing] : 372/375 ~= 99.2
Accuracy[Triangle] : 371/375 ~= 98.93333
Accuracy[Twister] : 370/375 ~= 98.66666
Accuracy[Whip] : 372/375 ~= 99.2
Accuracy[Zorro] : 372/375 ~= 99.2

Overall Accuracy: 9280/9375 ~= 98.98666, 	[Min: 97.6, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(5)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 372/375 ~= 99.2
Accuracy[Circle] : 369/375 ~= 98.4
Accuracy[Forward] : 371/375 ~= 98.93333
Accuracy[GolfSwing] : 370/375 ~= 98.66666
Accuracy[Infinity] : 371/375 ~= 98.93333
Accuracy[Lasso] : 371/375 ~= 98.93333
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 370/375 ~= 98.66666
Accuracy[LineToLeft] : 374/375 ~= 99.73333
Accuracy[LineToRight] : 373/375 ~= 99.46667
Accuracy[LineUp] : 369/375 ~= 98.4
Accuracy[OpenDoor] : 372/375 ~= 99.2
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 369/375 ~= 98.4
Accuracy[Slash] : 372/375 ~= 99.2
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 369/375 ~= 98.4
Accuracy[Square] : 373/375 ~= 99.46667
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 371/375 ~= 98.93333
Accuracy[TennisSwing] : 373/375 ~= 99.46667
Accuracy[Triangle] : 371/375 ~= 98.93333
Accuracy[Twister] : 368/375 ~= 98.13333
Accuracy[Whip] : 373/375 ~= 99.46667
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9286/9375 ~= 99.05067, 	[Min: 98.13333, Max:99.73333]
*********************************************************************************
*******************************   MERGED RESULTS(6)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 371/375 ~= 98.93333
Accuracy[Circle] : 370/375 ~= 98.66666
Accuracy[Forward] : 374/375 ~= 99.73333
Accuracy[GolfSwing] : 368/375 ~= 98.13333
Accuracy[Infinity] : 371/375 ~= 98.93333
Accuracy[Lasso] : 370/375 ~= 98.66666
Accuracy[Left] : 373/375 ~= 99.46667
Accuracy[LineDown] : 372/375 ~= 99.2
Accuracy[LineToLeft] : 368/375 ~= 98.13333
Accuracy[LineToRight] : 370/375 ~= 98.66666
Accuracy[LineUp] : 375/375 ~= 100
Accuracy[OpenDoor] : 374/375 ~= 99.73333
Accuracy[Parry] : 375/375 ~= 100
Accuracy[Right] : 370/375 ~= 98.66666
Accuracy[Slash] : 375/375 ~= 100
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 372/375 ~= 99.2
Accuracy[Square] : 372/375 ~= 99.2
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 365/375 ~= 97.33334
Accuracy[TennisSwing] : 373/375 ~= 99.46667
Accuracy[Triangle] : 372/375 ~= 99.2
Accuracy[Twister] : 375/375 ~= 100
Accuracy[Whip] : 370/375 ~= 98.66666
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9296/9375 ~= 99.15733, 	[Min: 97.33334, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(7)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 372/375 ~= 99.2
Accuracy[Circle] : 369/375 ~= 98.4
Accuracy[Forward] : 372/375 ~= 99.2
Accuracy[GolfSwing] : 374/375 ~= 99.73333
Accuracy[Infinity] : 372/375 ~= 99.2
Accuracy[Lasso] : 367/375 ~= 97.86667
Accuracy[Left] : 373/375 ~= 99.46667
Accuracy[LineDown] : 372/375 ~= 99.2
Accuracy[LineToLeft] : 370/375 ~= 98.66666
Accuracy[LineToRight] : 374/375 ~= 99.73333
Accuracy[LineUp] : 374/375 ~= 99.73333
Accuracy[OpenDoor] : 373/375 ~= 99.46667
Accuracy[Parry] : 373/375 ~= 99.46667
Accuracy[Right] : 369/375 ~= 98.4
Accuracy[Slash] : 371/375 ~= 98.93333
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 373/375 ~= 99.46667
Accuracy[Square] : 373/375 ~= 99.46667
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 371/375 ~= 98.93333
Accuracy[TennisSwing] : 374/375 ~= 99.73333
Accuracy[Triangle] : 373/375 ~= 99.46667
Accuracy[Twister] : 371/375 ~= 98.93333
Accuracy[Whip] : 371/375 ~= 98.93333
Accuracy[Zorro] : 375/375 ~= 100

Overall Accuracy: 9303/9375 ~= 99.232, 	[Min: 97.86667, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(8)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 370/375 ~= 98.66666
Accuracy[Circle] : 367/375 ~= 97.86667
Accuracy[Forward] : 366/375 ~= 97.6
Accuracy[GolfSwing] : 371/375 ~= 98.93333
Accuracy[Infinity] : 372/375 ~= 99.2
Accuracy[Lasso] : 362/375 ~= 96.53333
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 369/375 ~= 98.4
Accuracy[LineToLeft] : 369/375 ~= 98.4
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 371/375 ~= 98.93333
Accuracy[OpenDoor] : 374/375 ~= 99.73333
Accuracy[Parry] : 375/375 ~= 100
Accuracy[Right] : 370/375 ~= 98.66666
Accuracy[Slash] : 375/375 ~= 100
Accuracy[Slice] : 371/375 ~= 98.93333
Accuracy[Spike] : 372/375 ~= 99.2
Accuracy[Square] : 374/375 ~= 99.73333
Accuracy[Stab] : 371/375 ~= 98.93333
Accuracy[Stop] : 370/375 ~= 98.66666
Accuracy[TennisSwing] : 373/375 ~= 99.46667
Accuracy[Triangle] : 373/375 ~= 99.46667
Accuracy[Twister] : 372/375 ~= 99.2
Accuracy[Whip] : 370/375 ~= 98.66666
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9272/9375 ~= 98.90134, 	[Min: 96.53333, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(9)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 372/375 ~= 99.2
Accuracy[Circle] : 370/375 ~= 98.66666
Accuracy[Forward] : 371/375 ~= 98.93333
Accuracy[GolfSwing] : 369/375 ~= 98.4
Accuracy[Infinity] : 373/375 ~= 99.46667
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 370/375 ~= 98.66666
Accuracy[LineToLeft] : 371/375 ~= 98.93333
Accuracy[LineToRight] : 372/375 ~= 99.2
Accuracy[LineUp] : 375/375 ~= 100
Accuracy[OpenDoor] : 373/375 ~= 99.46667
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 368/375 ~= 98.13333
Accuracy[Slash] : 375/375 ~= 100
Accuracy[Slice] : 372/375 ~= 99.2
Accuracy[Spike] : 370/375 ~= 98.66666
Accuracy[Square] : 371/375 ~= 98.93333
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 369/375 ~= 98.4
Accuracy[TennisSwing] : 370/375 ~= 98.66666
Accuracy[Triangle] : 367/375 ~= 97.86667
Accuracy[Twister] : 369/375 ~= 98.4
Accuracy[Whip] : 369/375 ~= 98.4
Accuracy[Zorro] : 375/375 ~= 100

Overall Accuracy: 9280/9375 ~= 98.98666, 	[Min: 97.86667, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(10)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 373/375 ~= 99.46667
Accuracy[Circle] : 372/375 ~= 99.2
Accuracy[Forward] : 372/375 ~= 99.2
Accuracy[GolfSwing] : 371/375 ~= 98.93333
Accuracy[Infinity] : 370/375 ~= 98.66666
Accuracy[Lasso] : 370/375 ~= 98.66666
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 375/375 ~= 100
Accuracy[LineToLeft] : 370/375 ~= 98.66666
Accuracy[LineToRight] : 374/375 ~= 99.73333
Accuracy[LineUp] : 371/375 ~= 98.93333
Accuracy[OpenDoor] : 374/375 ~= 99.73333
Accuracy[Parry] : 375/375 ~= 100
Accuracy[Right] : 370/375 ~= 98.66666
Accuracy[Slash] : 374/375 ~= 99.73333
Accuracy[Slice] : 372/375 ~= 99.2
Accuracy[Spike] : 372/375 ~= 99.2
Accuracy[Square] : 374/375 ~= 99.73333
Accuracy[Stab] : 373/375 ~= 99.46667
Accuracy[Stop] : 370/375 ~= 98.66666
Accuracy[TennisSwing] : 373/375 ~= 99.46667
Accuracy[Triangle] : 367/375 ~= 97.86667
Accuracy[Twister] : 371/375 ~= 98.93333
Accuracy[Whip] : 370/375 ~= 98.66666
Accuracy[Zorro] : 374/375 ~= 99.73333

Overall Accuracy: 9298/9375 ~= 99.17867, 	[Min: 97.86667, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(11)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 374/375 ~= 99.73333
Accuracy[Circle] : 372/375 ~= 99.2
Accuracy[Forward] : 370/375 ~= 98.66666
Accuracy[GolfSwing] : 370/375 ~= 98.66666
Accuracy[Infinity] : 370/375 ~= 98.66666
Accuracy[Lasso] : 370/375 ~= 98.66666
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 372/375 ~= 99.2
Accuracy[LineToLeft] : 371/375 ~= 98.93333
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 374/375 ~= 99.73333
Accuracy[OpenDoor] : 372/375 ~= 99.2
Accuracy[Parry] : 373/375 ~= 99.46667
Accuracy[Right] : 371/375 ~= 98.93333
Accuracy[Slash] : 369/375 ~= 98.4
Accuracy[Slice] : 370/375 ~= 98.66666
Accuracy[Spike] : 369/375 ~= 98.4
Accuracy[Square] : 372/375 ~= 99.2
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 372/375 ~= 99.2
Accuracy[TennisSwing] : 373/375 ~= 99.46667
Accuracy[Triangle] : 370/375 ~= 98.66666
Accuracy[Twister] : 372/375 ~= 99.2
Accuracy[Whip] : 370/375 ~= 98.66666
Accuracy[Zorro] : 375/375 ~= 100

Overall Accuracy: 9288/9375 ~= 99.072, 	[Min: 98.4, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(12)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 370/375 ~= 98.66666
Accuracy[Circle] : 371/375 ~= 98.93333
Accuracy[Forward] : 369/375 ~= 98.4
Accuracy[GolfSwing] : 365/375 ~= 97.33334
Accuracy[Infinity] : 369/375 ~= 98.4
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 370/375 ~= 98.66666
Accuracy[LineDown] : 372/375 ~= 99.2
Accuracy[LineToLeft] : 368/375 ~= 98.13333
Accuracy[LineToRight] : 374/375 ~= 99.73333
Accuracy[LineUp] : 372/375 ~= 99.2
Accuracy[OpenDoor] : 375/375 ~= 100
Accuracy[Parry] : 372/375 ~= 99.2
Accuracy[Right] : 369/375 ~= 98.4
Accuracy[Slash] : 371/375 ~= 98.93333
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 370/375 ~= 98.66666
Accuracy[Square] : 373/375 ~= 99.46667
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 373/375 ~= 99.46667
Accuracy[TennisSwing] : 369/375 ~= 98.4
Accuracy[Triangle] : 370/375 ~= 98.66666
Accuracy[Twister] : 368/375 ~= 98.13333
Accuracy[Whip] : 370/375 ~= 98.66666
Accuracy[Zorro] : 374/375 ~= 99.73333

Overall Accuracy: 9271/9375 ~= 98.89066, 	[Min: 97.33334, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(13)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 371/375 ~= 98.93333
Accuracy[Circle] : 371/375 ~= 98.93333
Accuracy[Forward] : 372/375 ~= 99.2
Accuracy[GolfSwing] : 370/375 ~= 98.66666
Accuracy[Infinity] : 373/375 ~= 99.46667
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 372/375 ~= 99.2
Accuracy[LineDown] : 372/375 ~= 99.2
Accuracy[LineToLeft] : 372/375 ~= 99.2
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 375/375 ~= 100
Accuracy[OpenDoor] : 374/375 ~= 99.73333
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 374/375 ~= 99.73333
Accuracy[Slash] : 373/375 ~= 99.46667
Accuracy[Slice] : 375/375 ~= 100
Accuracy[Spike] : 369/375 ~= 98.4
Accuracy[Square] : 374/375 ~= 99.73333
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 369/375 ~= 98.4
Accuracy[TennisSwing] : 374/375 ~= 99.73333
Accuracy[Triangle] : 369/375 ~= 98.4
Accuracy[Twister] : 369/375 ~= 98.4
Accuracy[Whip] : 371/375 ~= 98.93333
Accuracy[Zorro] : 369/375 ~= 98.4

Overall Accuracy: 9297/9375 ~= 99.168, 	[Min: 98.4, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(14)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 371/375 ~= 98.93333
Accuracy[Circle] : 367/375 ~= 97.86667
Accuracy[Forward] : 373/375 ~= 99.46667
Accuracy[GolfSwing] : 368/375 ~= 98.13333
Accuracy[Infinity] : 371/375 ~= 98.93333
Accuracy[Lasso] : 371/375 ~= 98.93333
Accuracy[Left] : 374/375 ~= 99.73333
Accuracy[LineDown] : 374/375 ~= 99.73333
Accuracy[LineToLeft] : 371/375 ~= 98.93333
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 370/375 ~= 98.66666
Accuracy[OpenDoor] : 373/375 ~= 99.46667
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 371/375 ~= 98.93333
Accuracy[Slash] : 372/375 ~= 99.2
Accuracy[Slice] : 372/375 ~= 99.2
Accuracy[Spike] : 372/375 ~= 99.2
Accuracy[Square] : 372/375 ~= 99.2
Accuracy[Stab] : 370/375 ~= 98.66666
Accuracy[Stop] : 368/375 ~= 98.13333
Accuracy[TennisSwing] : 372/375 ~= 99.2
Accuracy[Triangle] : 370/375 ~= 98.66666
Accuracy[Twister] : 370/375 ~= 98.66666
Accuracy[Whip] : 373/375 ~= 99.46667
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9283/9375 ~= 99.01867, 	[Min: 97.86667, Max:99.73333]
*********************************************************************************
*******************************   MERGED RESULTS(15)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 372/375 ~= 99.2
Accuracy[Circle] : 369/375 ~= 98.4
Accuracy[Forward] : 370/375 ~= 98.66666
Accuracy[GolfSwing] : 372/375 ~= 99.2
Accuracy[Infinity] : 371/375 ~= 98.93333
Accuracy[Lasso] : 359/375 ~= 95.73333
Accuracy[Left] : 373/375 ~= 99.46667
Accuracy[LineDown] : 371/375 ~= 98.93333
Accuracy[LineToLeft] : 372/375 ~= 99.2
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 374/375 ~= 99.73333
Accuracy[OpenDoor] : 374/375 ~= 99.73333
Accuracy[Parry] : 375/375 ~= 100
Accuracy[Right] : 369/375 ~= 98.4
Accuracy[Slash] : 375/375 ~= 100
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 369/375 ~= 98.4
Accuracy[Square] : 373/375 ~= 99.46667
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 371/375 ~= 98.93333
Accuracy[TennisSwing] : 373/375 ~= 99.46667
Accuracy[Triangle] : 370/375 ~= 98.66666
Accuracy[Twister] : 370/375 ~= 98.66666
Accuracy[Whip] : 369/375 ~= 98.4
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9283/9375 ~= 99.01867, 	[Min: 95.73333, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(16)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 371/375 ~= 98.93333
Accuracy[Circle] : 373/375 ~= 99.46667
Accuracy[Forward] : 369/375 ~= 98.4
Accuracy[GolfSwing] : 372/375 ~= 99.2
Accuracy[Infinity] : 371/375 ~= 98.93333
Accuracy[Lasso] : 367/375 ~= 97.86667
Accuracy[Left] : 373/375 ~= 99.46667
Accuracy[LineDown] : 371/375 ~= 98.93333
Accuracy[LineToLeft] : 368/375 ~= 98.13333
Accuracy[LineToRight] : 367/375 ~= 97.86667
Accuracy[LineUp] : 374/375 ~= 99.73333
Accuracy[OpenDoor] : 375/375 ~= 100
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 373/375 ~= 99.46667
Accuracy[Slash] : 373/375 ~= 99.46667
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 373/375 ~= 99.46667
Accuracy[Square] : 371/375 ~= 98.93333
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 371/375 ~= 98.93333
Accuracy[TennisSwing] : 369/375 ~= 98.4
Accuracy[Triangle] : 373/375 ~= 99.46667
Accuracy[Twister] : 370/375 ~= 98.66666
Accuracy[Whip] : 370/375 ~= 98.66666
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9288/9375 ~= 99.072, 	[Min: 97.86667, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(17)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 372/375 ~= 99.2
Accuracy[Circle] : 372/375 ~= 99.2
Accuracy[Forward] : 369/375 ~= 98.4
Accuracy[GolfSwing] : 374/375 ~= 99.73333
Accuracy[Infinity] : 374/375 ~= 99.73333
Accuracy[Lasso] : 364/375 ~= 97.06667
Accuracy[Left] : 374/375 ~= 99.73333
Accuracy[LineDown] : 373/375 ~= 99.46667
Accuracy[LineToLeft] : 368/375 ~= 98.13333
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 372/375 ~= 99.2
Accuracy[OpenDoor] : 375/375 ~= 100
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 368/375 ~= 98.13333
Accuracy[Slash] : 373/375 ~= 99.46667
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 374/375 ~= 99.73333
Accuracy[Square] : 373/375 ~= 99.46667
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 372/375 ~= 99.2
Accuracy[TennisSwing] : 372/375 ~= 99.2
Accuracy[Triangle] : 369/375 ~= 98.4
Accuracy[Twister] : 374/375 ~= 99.73333
Accuracy[Whip] : 373/375 ~= 99.46667
Accuracy[Zorro] : 373/375 ~= 99.46667

Overall Accuracy: 9300/9375 ~= 99.2, 	[Min: 97.06667, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(18)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 374/375 ~= 99.73333
Accuracy[Circle] : 371/375 ~= 98.93333
Accuracy[Forward] : 374/375 ~= 99.73333
Accuracy[GolfSwing] : 369/375 ~= 98.4
Accuracy[Infinity] : 372/375 ~= 99.2
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 371/375 ~= 98.93333
Accuracy[LineToLeft] : 371/375 ~= 98.93333
Accuracy[LineToRight] : 371/375 ~= 98.93333
Accuracy[LineUp] : 373/375 ~= 99.46667
Accuracy[OpenDoor] : 375/375 ~= 100
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 371/375 ~= 98.93333
Accuracy[Slash] : 375/375 ~= 100
Accuracy[Slice] : 374/375 ~= 99.73333
Accuracy[Spike] : 371/375 ~= 98.93333
Accuracy[Square] : 372/375 ~= 99.2
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 372/375 ~= 99.2
Accuracy[TennisSwing] : 374/375 ~= 99.73333
Accuracy[Triangle] : 370/375 ~= 98.66666
Accuracy[Twister] : 371/375 ~= 98.93333
Accuracy[Whip] : 374/375 ~= 99.73333
Accuracy[Zorro] : 372/375 ~= 99.2

Overall Accuracy: 9305/9375 ~= 99.25333, 	[Min: 98.4, Max:100]
*********************************************************************************
*******************************   MERGED RESULTS(19)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 373/375 ~= 99.46667
Accuracy[Circle] : 372/375 ~= 99.2
Accuracy[Forward] : 369/375 ~= 98.4
Accuracy[GolfSwing] : 369/375 ~= 98.4
Accuracy[Infinity] : 370/375 ~= 98.66666
Accuracy[Lasso] : 369/375 ~= 98.4
Accuracy[Left] : 373/375 ~= 99.46667
Accuracy[LineDown] : 373/375 ~= 99.46667
Accuracy[LineToLeft] : 374/375 ~= 99.73333
Accuracy[LineToRight] : 372/375 ~= 99.2
Accuracy[LineUp] : 370/375 ~= 98.66666
Accuracy[OpenDoor] : 372/375 ~= 99.2
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 373/375 ~= 99.46667
Accuracy[Slash] : 374/375 ~= 99.73333
Accuracy[Slice] : 373/375 ~= 99.46667
Accuracy[Spike] : 368/375 ~= 98.13333
Accuracy[Square] : 373/375 ~= 99.46667
Accuracy[Stab] : 374/375 ~= 99.73333
Accuracy[Stop] : 371/375 ~= 98.93333
Accuracy[TennisSwing] : 372/375 ~= 99.2
Accuracy[Triangle] : 370/375 ~= 98.66666
Accuracy[Twister] : 372/375 ~= 99.2
Accuracy[Whip] : 368/375 ~= 98.13333
Accuracy[Zorro] : 372/375 ~= 99.2

Overall Accuracy: 9290/9375 ~= 99.09333, 	[Min: 98.13333, Max:99.73333]
*********************************************************************************
*******************************   MERGED RESULTS(20)   ***************************
Training Samples Recognition Accuracy

Accuracy[Chop] : 372/375 ~= 99.2
Accuracy[Circle] : 371/375 ~= 98.93333
Accuracy[Forward] : 370/375 ~= 98.66666
Accuracy[GolfSwing] : 370/375 ~= 98.66666
Accuracy[Infinity] : 370/375 ~= 98.66666
Accuracy[Lasso] : 370/375 ~= 98.66666
Accuracy[Left] : 371/375 ~= 98.93333
Accuracy[LineDown] : 370/375 ~= 98.66666
Accuracy[LineToLeft] : 372/375 ~= 99.2
Accuracy[LineToRight] : 369/375 ~= 98.4
Accuracy[LineUp] : 370/375 ~= 98.66666
Accuracy[OpenDoor] : 373/375 ~= 99.46667
Accuracy[Parry] : 374/375 ~= 99.73333
Accuracy[Right] : 370/375 ~= 98.66666
Accuracy[Slash] : 375/375 ~= 100
Accuracy[Slice] : 375/375 ~= 100
Accuracy[Spike] : 372/375 ~= 99.2
Accuracy[Square] : 372/375 ~= 99.2
Accuracy[Stab] : 375/375 ~= 100
Accuracy[Stop] : 373/375 ~= 99.46667
Accuracy[TennisSwing] : 369/375 ~= 98.4
Accuracy[Triangle] : 373/375 ~= 99.46667
Accuracy[Twister] : 369/375 ~= 98.4
Accuracy[Whip] : 371/375 ~= 98.93333
Accuracy[Zorro] : 374/375 ~= 99.73333

Overall Accuracy: 9290/9375 ~= 99.09333, 	[Min: 98.4, Max:100]
*********************************************************************************
************************OVERALL ACCURACY*****************************************
Training Samples Recognition Accuracy

Accuracy[Chop] ~= 99.13332
Accuracy[Circle] ~= 98.76
Accuracy[Forward] ~= 98.82666
Accuracy[GolfSwing] ~= 98.67999
Accuracy[Infinity] ~= 98.97334
Accuracy[Lasso] ~= 98.08
Accuracy[Left] ~= 99.22666
Accuracy[LineDown] ~= 99.17332
Accuracy[LineToLeft] ~= 98.77332
Accuracy[LineToRight] ~= 99.08
Accuracy[LineUp] ~= 99.34665
Accuracy[OpenDoor] ~= 99.62666
Accuracy[Parry] ~= 99.70665
Accuracy[Right] ~= 98.78667
Accuracy[Slash] ~= 99.56
Accuracy[Slice] ~= 99.41333
Accuracy[Spike] ~= 98.93333
Accuracy[Square] ~= 99.42666
Accuracy[Stab] ~= 99.69332
Accuracy[Stop] ~= 98.82667
Accuracy[TennisSwing] ~= 99.17332
Accuracy[Triangle] ~= 98.75999
Accuracy[Twister] ~= 98.86666
Accuracy[Whip] ~= 98.93333
Accuracy[Zorro] ~= 99.51999

Overall Accuracy: ~= 99.0912, 	[Min: 98.08, Max:99.70665]
Experiment Completed in 0.72826128 minutes
